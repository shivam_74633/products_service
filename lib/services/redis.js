'use strict';

const Redis = require('redis');

const { promisify } = require('util');

const client = Redis.createClient({
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT
});

//For Adding and getting Data
const PUSH_TOKEN = promisify(client.rpush).bind(client);
const GET_TOKEN = promisify(client.lrange).bind(client);
const SET = promisify(client.set).bind(client);
const GET = promisify(client.get).bind(client);


//For deleting Data
const DELETE_TOKEN = promisify(client.lrem).bind(client);
const DELETE = promisify(client.del).bind(client);


const storeRefreshToken = async (key,tokenID) => {
    await PUSH_TOKEN([key,tokenID]);
}

const checkRefreshTokenPresent = async (key,tokenID) => {
    const tokensFromRedis = await GET_TOKEN(key,0,-1);
    if(tokensFromRedis.includes(tokenID)) return true
    else return false;
}

const checkLoginLimitExceed = async (key,limit=5) => {
    const tokensFromRedis = await GET_TOKEN(key,0,-1);
    return tokensFromRedis.length >= 5;
}

const expireToken = async (key,tokenID) => {
    await DELETE_TOKEN(key,1,tokenID);
}

const blackListToken = async (key) => {
    await SET(key,'BLACKLISTED','EX',process.env.OTP_REDIS_EXPIRE_TIME);
}

const checkIfTokenBlacklisted = async (key) => {
    const data = await GET(key);
    if(data) return true
    return false;
}

const expireAllTokens = async (key) => {
    await DELETE(key);
}


module.exports = {  
    storeRefreshToken,
    checkRefreshTokenPresent,
    expireToken,
    blackListToken,
    checkIfTokenBlacklisted,
    expireToken,
    checkLoginLimitExceed,
    expireAllTokens
}