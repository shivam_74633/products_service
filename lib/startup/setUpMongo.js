'use strict';

const mongoose = require('mongoose');

module.exports = async function () {
    return new Promise((resolve,reject) => {
        let mongodb = process.env.MONGODB_URL;
        mongoose.connect(mongodb, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        })
        .then( () => {
            console.log('Databse connected!!');
            resolve();
        })
        .catch( (err) => {
            console.error('Database connection Error!!',err.message);
            process.exit();
        })
    })
}