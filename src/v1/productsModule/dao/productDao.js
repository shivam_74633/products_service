const Products = require('../models/products');

const getProductById = async (id) => {
    const product = Products.findById(id);
    if(!product) {
        return null
    }
    return product
}


const saveProduct = async (product) => {
    const newProduct = new Products(product);
    const savedProduct = await newProduct.save();
    if(!savedProduct) {
        return null
    }
    return savedProduct
}


const updateProduct = async (id,updateProduct) => {

}




module.exports = {
    getProductById,
    saveProduct,
    updateProduct
}


