'use strict';

const {mongo} = require('../../lib').startup;

const configureStartup = async () => {
        // Connect Mongo
        await mongo();
        
}


module.exports = configureStartup;