'use strict';

const boot = async (rootDir,app) => {
    try{
        await require('./startUpsetup')();

        await require('./apiSetup')(app,rootDir);
    }
    catch (err) {
        // Notify by email
        console.log(err);
        app.use((req,res) => {
            res.status(503).send('Service Unavailable');
        })
    }
}


module.exports = boot